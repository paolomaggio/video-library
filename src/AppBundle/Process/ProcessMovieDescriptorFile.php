<?php

namespace AppBundle\Process;

use Symfony\Component\Finder\Finder;
use AppBundle\Entity\Movie;
use AppBundle\Entity\Genre;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Samba\SambaStreamWrapper;

/**
 * ProcessMovieDescriptorFile.
 */
class ProcessMovieDescriptorFile
{
    private $container;

    /* @var Movie $movie */
    private $movie;
    private $title;
    private $year;
    private $dateAdded;
    private $originalTitle;
    private $runtime;
    private $genres;
    private $idIMDB;
    private $plot;
    private $tagline;
    private $director;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function scanAndProcess()
    {

        // Da usare solo in sistema operativo Linux
        // SambaStreamWrapper::register();

        $finder = new Finder();

        $logger = $this->container->get('logger');

        // in ambiente Windows : \\\\disubbi_nas2\\Videoteca
        // in ambiente Linux : smb://admin:bianca01@192.168.1.34/Archivio

        $finder->files()->name('*.nfo')->in('\\\\disubbi_nas2\\Videoteca\\Film');
        /*
        foreach ($finder as $file) {
            set_time_limit(120);
            dump($file->getRealPath());
        }
        die();
        */

        // $listFiles = $finder->in('smb://admin.bianca01@192.168.1.70/Videoteca/Film')->name('*.nfo')->files();
        foreach ($finder as $file) {
            set_time_limit(120);

            $logger->info('Esame file '.$file->getRealPath());

            $fileName = $file->getRealPath();
            $xml = simplexml_load_file($fileName);
            $this->getXMLElements($xml);

            $this->movie = $this->container->get('doctrine')
                ->getRepository('AppBundle:Movie')
                ->findOneBy(['title' => $this->title]);

            if (!$this->movie) {
                $this->movie = new Movie();
                $this->movie->setTitle($this->title);
            } else {
                $this->container->get('doctrine')->getRepository('AppBundle:Genre')
                    ->deleteAllMovieGenres($this->movie);
            }
            $this->setMovieProperties();
            $this->setMovieGenres();

            $this->container->get('doctrine')->getManager()->persist($this->movie);
            $this->container->get('doctrine')->getManager()->flush();
        }
    }

    private function getXMLElements($xml)
    {
        $this->title = (string) $xml->title;
        $this->year = (string) $xml->year;
        $this->dateAdded = (string) $xml->dateadded;
        $this->originalTitle = (string) $xml->originaltitle;
        $this->runtime = (int) $xml->runtime;
        $this->genres = (array) $xml->genre;
        $this->idIMDB = (string) $xml->id;
        $this->plot = (string) $xml->plot;
        $this->tagline = isset($xml->tagline) ? (string) $xml->tagline : '';
        $this->director = (string) $xml->director;
        $this->dateAdded = (string) $xml->dateadded;
    }

    private function setMovieProperties()
    {
        $this->movie->setYear($this->year);
        $this->movie->setOriginalTitle($this->originalTitle);
        $this->movie->setRuntime($this->runtime);
        $this->movie->setIdIMDB($this->idIMDB);
        $this->movie->setPlot($this->plot);
        $this->movie->setTagline($this->tagline);
        $this->movie->setDirector($this->director);
        $this->movie->setDateAdded(new \DateTime($this->dateAdded));
    }

    private function setMovieGenres()
    {
        $this->movie->clearGenres();

        $i = count($this->genres);
        for ($j = 0; $j < $i; ++$j) {
            $genre = new Genre();
            $genre->setGenre($this->genres[$j]);
            $genre->setMovie($this->movie);
            $this->movie->addGenre($genre);
            $this->container->get('doctrine')->getManager()->persist($genre);
        }
    }
}
