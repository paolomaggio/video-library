<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class GenresRepository extends EntityRepository
{
    public function deleteAllMovieGenres($movie)
    {
        $this->createQueryBuilder('genres')
            ->delete()
            ->where('genres.movie = :movie')
            ->setParameter('movie', $movie)
            ->getQuery()
            ->execute();
    }
}
