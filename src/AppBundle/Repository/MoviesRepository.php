<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class MoviesRepository extends EntityRepository
{
    public function getMoviesByGenre($genre)
    {
        return $this->getEntityManager()->createQuery(
            'select movies.id, movies.title, movies.year, movies.director, movies.dateAdded, genre.genre 
              from AppBundle:Genre genre
              join genre.movie movies 
              where genre.genre = :genre
              order by movies.year')
            ->setParameter('genre', $genre);
    }

    public function getMoviesByDirector($director)
    {
        return $this->createQueryBuilder("movies")
            ->where("movies.director = :director")
            ->setParameter('director', $director)
            ->orderBy("movies.year")
            ->getQuery();
    }

    public function getMoviesByYear($year)
    {
        return $this->createQueryBuilder("movies")
            ->select('movies')
            ->where("movies.year = :year")
            ->setParameter('year', $year)
            ->orderBy("movies.title")
            ->getQuery()->getResult();
    }

    public function getMoviesYearsAsArray()
    {
        $list1 = $this->createQueryBuilder("movies")
            ->select('movies.year')
            ->orderBy('movies.year')
            ->distinct()
            ->getQuery()
            ->getResult();
        $list2 = [];
        for ($j=0; $j < count($list1); $j++) {
            $list2[strval($list1[$j]['year'])] = strval($list1[$j]['year']);
        }
        return $list2;
    }
}
