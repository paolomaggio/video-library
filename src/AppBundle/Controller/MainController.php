<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Process\ProcessMovieDescriptorFile;

/**
 * MainController.
 *
 * @author Utente
 */
class MainController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // $a = new ProcessMovieDescriptorFile($this->container);
        // $a->scanAndProcess();

        return $this->redirectToRoute('movie_index', ['order' => 'title']);
        // return $this->render('da_implementare.html.twig');
    }
}
