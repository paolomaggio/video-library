<?php

namespace AppBundle\Controller;

use AppBundle\Form\FilterByYearType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Process\ProcessMovieDescriptorFile;

/**
 * MovieController.
 *
 * @Route("/movie")
 */
class MovieController extends Controller
{
    /**
     * @Route("/index/{order}", name="movie_index")
     */
    public function indexAction($order, Request $request)
    {
        $query = $this->getDoctrine()->getRepository('AppBundle:Movie')
            ->createQueryBuilder('m');

        if ($order == 'title') {
            $query = $query->orderBy("m.title");
        }

        if ($order == 'year') {
            $query = $query->orderBy("m.year");
        }

        if ($order == 'director') {
            $query = $query->orderBy("m.director");
        }

        if ($order == 'dateadded') {
            $query = $query->orderBy("m.dateAdded", "DESC");
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query->getQuery(),
            $request->query->getInt('page', 1),
            30
        );

        return $this->render('movie/list.html.twig', [
                'movieList' => $pagination,
                'order' => $order,
            ]
        );
    }

    /**
     * @Route("/by_genre/{genre}", name="movies_by_genre")
     */
    public function moviesByGenreAction($genre, Request $request)
    {
        $query = $this->getDoctrine()->getRepository('AppBundle:Movie')
            ->getMoviesByGenre($genre);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render(':movie:list_by_genre.html.twig',
            ['movieList' => $pagination,
                'genre' => $genre]);
    }

    /**
     * @Route("/by_director/{director}", name="movies_by_director")
     */
    public function moviesByDirectorAction($director, Request $request)
    {
        $query = $this->getDoctrine()->getRepository('AppBundle:Movie')
            ->getMoviesByDirector($director);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            30
        );

        return $this->render(':movie:list_by_director.html.twig',
            ['movieList' => $pagination,
                'director' => $director]);
    }

    /**
     * @Route("/detail/{id}", name="movie_detail")
     */
    public function moviesDetailAction($id, Request $request)
    {
        $movie = $this->getDoctrine()->getRepository('AppBundle:Movie')
            ->find($id);
        return $this->render(':movie:detail.html.twig',
            ['movie' => $movie,]);
    }

    /**
     * @Route("/filter_by_year/{year}", name="movie_filter_by_year", defaults={"year" = ""})
     */
    public function moviesFilterByYearAction($year, Request $request)
    {
        $movies = null;

        $form = $this->createForm(FilterByYearType::class, null,
            ['entity_manager' => $this->get('doctrine.orm.entity_manager')]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $aaa = $form['year']->getData();
            $year = $form['year']->getData();
            var_dump($year);
            // die();
            $movies = $this->getDoctrine()->getRepository("AppBundle:Movie")
                ->getMoviesByYear($year);
        }
        // var_dump($movies);

        return $this->render(':movie:filter_by_year.html.twig',
            ['form' => $form->createView(),
                'year' => $year,
                'movies' => $movies,
            ]);
    }

    /**
     * @Route("/scan", name="movie_scan")
     */
    public function moviesScanAction(Request $request)
    {
        $a = new ProcessMovieDescriptorFile($this->container);
        $a->scanAndProcess();
        return $this->redirectToRoute('movie_index', ['order' => 'title']);
    }
}
