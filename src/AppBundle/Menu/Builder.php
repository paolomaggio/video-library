<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;

class Builder
{
    public function mainMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(['class' => 'nav navbar-nav']);
        $menu->addChild('Home',
            ['route' => 'movie_index',
             'routeParameters' => ['order' => 'title']]
            );

        $menu->addChild('Elenco film')->setAttribute('dropdown', true);
        $menu['Elenco film']->addChild('Elenco per titolo',
            ['route' => 'movie_index',
                'routeParameters' => ['order' => 'title']]
            );
        $menu['Elenco film']->addChild('Elenco per anno',
            ['route' => 'movie_index',
                'routeParameters' => ['order' => 'year']]
        );
        $menu['Elenco film']->addChild('Elenco per regista',
            ['route' => 'movie_index',
                'routeParameters' => ['order' => 'director']]
        );

        $menu['Elenco film']->addChild('Filtro per anno',
            ['route' => 'movie_filter_by_year',
                ]
        );
        return $menu;
    }
}
