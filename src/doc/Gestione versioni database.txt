La gestione delle migrazioni è in alternativa rispetto
all'aggiornamento del database effettuato direttamente
da doctrine con i comandi
php bin\console doctrine:schema:update --force

Per generare una nuova versione (migrazione) :
php bin\console doctrine:migrations:diff

Nota : la generazione deve essere lanciata dopo aver aggiornato l'entità

Per applicare la nuova versione (migrazione) :
php bin\console doctrine:migrations:migrate
